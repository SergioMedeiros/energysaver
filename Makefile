#
#Copyright (C) 2020  Sergio Medeiros García
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>
#

default:
	@clear
	@echo " "
	@echo "Opciones de cofiguración del servidor Energysaver:"
	@echo " - build-server       : Instalación completa [Recomendado]"
	@echo "                        Instala mongod, httpd, NodeJS, npm, angular-cli y configura crontab, el firewall y una IP estática."
	@echo " - install-mongo      : Instala mongod y crea una base de datos 'energysaver'." 
	@echo " - install-api        : Instala mongod y despliega su API." 
	@echo " - install-web        : Instala mongod, apache y despliega la web y la API."
	@echo " "
	@echo "Opciones de configuración del cliente Energysaver:"
	@echo " - build-client       : Instalación completa del cliente (Ubuntu)."

server-header:
	@clear
	@echo " "
	@echo "Bienvenido a ENERGYSAVER!!"
	@echo " "
	@echo "Mi nombre es Sergio y voy a instalar algunas cosas"
	@echo "en tu CentOS 8 para convertirlo en un servidor Energysaver:"
	@echo " "
	@echo " 0. actualizar paquetes (yum update)" 
	@echo " 1. mongod" 
	@echo " 2. nodejs" 
	@echo " 3. npm" 
	@echo " 4. angular-cli" 
	@echo " 5. httpd (apache)" 
	@echo " 6. abrir puertos 80, 443 y 3000 en el firewall" 
	@echo " 7. configurar IP estática"
	@echo " "
	@echo "Este proceso puede tardar unos minutos, solo necesito"
	@echo "provilegios de administrador ;)"

client-header:
	@clear
	@echo " "
	@echo "Bienvenido a ENERGYSAVER!!"
	@echo " "
	@echo "Mi nombre es Sergio y voy a instalar algunas cosas"
	@echo "en tu Ubuntu para convertirlo en un cliente Energysaver:"
	@echo " "
	@echo " 0. nodejs" 
	@echo " 1. npm" 
	@echo " 2. configurar tu tajeta de red" 
	@echo " 3. establecer energysaver.com en tu archivo /etc/hosts."
	@echo " "
	@echo "Este proceso puede tardar unos minutos,"
	@echo "necesitaré que me falicites unos datos:"

update-system:
	@sudo yum update -y
	@sudo timedatectl set-timezone Europe/Madrid
	
install-wol:
	@sh ./server-configuration/config-wol.sh

install-mongo: update-system install-wol
	@sh ./server-configuration/config-mongodb.sh

install-api: install-mongo
	@sh ./server-configuration/config-node.sh
	@sh ./server-configuration/config-crontab.sh

install-web: install-api
	@sh ./server-configuration/config-apache.sh

set-static-ip:
	@sh ./server-configuration/config-network.sh

build-server: server-header install-web set-static-ip
	@echo "ENERGYSAVER is ready!!"

set-server:
	@sh ./client-configuration/config-hosts.sh

install-runner:
	@sh ./client-configuration/config-node.sh
	@sh ./client-configuration/config-crontab.sh

set-wol:
	@sh ./client-configuration/config-wol.sh

build-client: client-header set-server install-runner set-wol
	@echo "Client is ready!!"
