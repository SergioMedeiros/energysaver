#
#Copyright (C) 2020  Sergio Medeiros García
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>
#

echo "¿Cual es la IP del servidor?"
read ip 
echo " "
echo "¿Estas seguro de que la ip del servidor es $ip?[y/n]"
read confirm

if [ $confirm = 'y' ]
then
    echo "$ip energysaver.com" | sudo tee -a /etc/hosts
    echo " "
    echo "Servidor registrado."
else
    echo "Finalizando el script sin hacer cambios."
fi