import { Component, OnInit } from '@angular/core';
import { LoginObject } from "../core/models/login-object.model";
import { AuthenticationService } from "../core/services/authentication.service";
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { StorageService } from "../core/services/storage.service";
import { Router } from "@angular/router";
import { User } from "../core/models/user.model";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
  public user: User = new User();
  public email: string = "";
  public password: string = "";
  public passwordTwo: string = "";
  public register: boolean = false;
  public creationGroup: FormGroup;
  public registerGroup: FormGroup;

  constructor(
    private authenticationService: AuthenticationService,
    private storageService: StorageService,
    private router: Router,
    private formBuilder: FormBuilder
  ) {
    this.creationGroup = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
    this.registerGroup = this.formBuilder.group({
      email: ['', Validators.required],
      nickname: ['', Validators.required],
      password: ['', Validators.required],
      passwordTwo: ['', Validators.required]
    });
  }

  ngOnInit() { }

  public signIn(): void {
    this.authenticationService.signIn(new LoginObject({ email: this.email, password: this.password })).subscribe(
      data => this.correctLogin(data),
      error => console.log(error)
    )
  }

  public signUp(): void {
    if (this.user.password !== this.passwordTwo) {
      alert("Las contraseñas deben ser idénticas.");
    } else {
      this.authenticationService.signUp(this.user).subscribe(
        data => this.correctLogin(data),
        error => console.log(error)
      )
    }
  }

  public changeView(): void {
    this.register = !this.register;
  }

  private correctLogin(data: any) {
    this.storageService.setCurrentSession(data);
    this.router.navigate(['/dashboard']);
  }
}
