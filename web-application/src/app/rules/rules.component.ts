import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from "../core/services/api.service";
import { StorageService } from "../core/services/storage.service";
import { NotifyerService } from "../core/services/notifyer.service";
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Rule } from '../core/models/rule.model';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-rules',
  templateUrl: './rules.component.html',
  styleUrls: ['./rules.component.css']
})

export class RulesComponent implements OnInit { //ICON power_settings_new // power // power_off
  //Punctual rules vars
  public displayedPunctual: string[] = ['active', 'type', 'target', 'user', 'time', 'day', 'options'];
  public punctualDataSource: MatTableDataSource<Object>;

  //Periodic rules vars
  public displayedPeriodic: string[] = ['active', 'type', 'target', 'user', 'period', 'time', 'initDate', 'endDate', 'options'];
  public periodicDataSource: MatTableDataSource<Object>;

  //Common vars
  public paginationOptions: number[] = [5, 10, 25, 100];
  public periodicRules: boolean = false;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(
    private route: ActivatedRoute,
    private apiService: ApiService,
    private storageService: StorageService,
    private notifyer: NotifyerService
  ) {
    this.loadRules();
    if (this.route.snapshot.params.type === 'periodic') {
      this.periodicRules = true;
    }
  }

  ngOnInit(): void { }

  applyFilter(event: Event, periodic: boolean) {
    if (periodic) {
      const filterValue = (event.target as HTMLInputElement).value;
      this.periodicDataSource.filter = filterValue.trim().toLowerCase();

      if (this.periodicDataSource.paginator) {
        this.periodicDataSource.paginator.firstPage();
      }
    } else {
      const filterValue = (event.target as HTMLInputElement).value;
      this.punctualDataSource.filter = filterValue.trim().toLowerCase();

      if (this.punctualDataSource.paginator) {
        this.punctualDataSource.paginator.firstPage();
      }
    }
  }

  public switchRules(): void {
    this.periodicRules = !this.periodicRules;
  }

  public deleteRule(id: string): void {
    if (confirm("¿Seguro que quieres eliminar esta regla?")) { //https://material.angular.io/components/dialog/examples
      this.apiService.deleteRule(id).subscribe(
        data => this.notifyAndReload(),
        error => console.log(error)
      )
    } else {
      this.notifyer.confirmSnackBar("La regla no se ha eliminado.");
    }
  }

  public enableOrDisableRule(rule: Rule): void {
    rule.active ? this.apiService.disableRule(rule).subscribe() : this.apiService.enableRule(rule).subscribe();
  }

  public parseTime(date: string): string{
    var parsedDate = new Date(date);
    return (parsedDate.getHours() < 10 ? "0" + parsedDate.getHours() : parsedDate.getHours()) +
    ":" + (parsedDate.getMinutes() < 10 ? "0" + parsedDate.getMinutes() : parsedDate.getMinutes());
  }

  private setPunctualData(data: Array<Object>): void {
    this.punctualDataSource = new MatTableDataSource(data);
    this.punctualDataSource.paginator = this.paginator;
    this.punctualDataSource.sort = this.sort;
  }

  private setPeriodicData(data: Array<Object>): void {
    this.periodicDataSource = new MatTableDataSource(data);
    this.periodicDataSource.paginator = this.paginator;
    this.periodicDataSource.sort = this.sort;
  }

  private loadRules(): void {
    var user = this.storageService.getCurrentUser();
    if (user.role === "admin") {
      this.apiService.getPunctualRules().subscribe(
        data => this.setPunctualData(data),
        error => console.log(error)
      )
      this.apiService.getPeriodicRules().subscribe(
        data => this.setPeriodicData(data),
        error => console.log(error)
      )
    } else {
      this.apiService.getPunctualRulesForUser(user.id).subscribe(
        data => this.setPunctualData(data),
        error => console.log(error)
      )
      this.apiService.getPeriodicRulesForUser(user.id).subscribe(
        data => this.setPeriodicData(data),
        error => console.log(error)
      )
    }
  }

  private notifyAndReload(): void {
    this.loadRules();
    this.notifyer.confirmSnackBar("La regla ha sido eliminada.");
  }
}