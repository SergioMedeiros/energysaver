import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from "../../core/services/api.service";
import { NotifyerService } from "../../core/services/notifyer.service";
import { Device } from '../../core/models/device.model';
import { Rule } from '../../core/models/rule.model';
import { User } from '../../core/models/user.model';
import { Group } from '../../core/models/group.model';
import { StorageService } from "../../core/services/storage.service";
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';

@Component({
  selector: 'app-rule',
  templateUrl: './rule.component.html',
  styleUrls: ['./rule.component.css'],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'es-ES' },
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },
  ],
})

export class RuleComponent implements OnInit {
  public rule: Rule;
  public currentUser: User;
  public devices: Array<Device>;
  public groups: Array<Group>;
  public formAction: string = 'Actualizar';
  public time = { hour: 0, minute: 0 };
  public backPath: string = '/dashboard/rules';
  public target: string = 'device';
  public period: any = {
    name: 'Todos los días',
    completed: false,
    days: [
      { name: 'Lunes', completed: false },
      { name: 'Martes', completed: false },
      { name: 'Miércoles', completed: false },
      { name: 'Jueves', completed: false },
      { name: 'Viernes', completed: false },
      { name: 'Sábado', completed: false },
      { name: 'Domingo', completed: false }
    ]
  };

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private apiService: ApiService,
    private storageService: StorageService,
    private notifyer: NotifyerService
  ) {
    this.currentUser = this.storageService.getCurrentUser();

    if (this.currentUser.role === 'admin') {
      this.apiService.getDevices().subscribe(
        data => this.devices = data,
        error => console.log(error)
      )
      this.apiService.getGroups().subscribe(
        data => this.groups = data,
        error => console.log(error)
      )
    } else {
      this.apiService.getGroupsFor(this.currentUser.id).subscribe(
        data => this.groups = data,
        error => console.log(error)
      )
      this.apiService.getDevicesFor(this.currentUser.id).subscribe(
        data => this.devices = data,
        error => console.log(error)
      )
    }

    if (this.route.snapshot.params.id !== undefined) {
      this.apiService.getRule(this.route.snapshot.params.id).subscribe(
        data => this.setRuleData(data),
        error => console.log(error)
      )
    } else {
      this.rule = new Rule();
      this.formAction = 'Crear';
    }
  }

  ngOnInit(): void { }

  public createOrUpdateRule(): void {
    if (this.passValidations()) {
      this.fillRule();
      if (this.formAction === 'Crear') {
        this.apiService.createRule(this.rule).subscribe(
          data => this.noticeUser(data),
          error => alert(error.error)
        )
      } else {
        this.apiService.updateRule(this.rule).subscribe(
          data => this.noticeUser(data),
          error => alert(error.error)
        )
      }
    }
  }

  public someDay(): boolean {
    if (this.period.days == null) {
      return false;
    }
    return this.period.days.filter(t => t.completed).length > 0 && !this.period.completed;
  }

  public updatePeriod() {
    this.period.completed = this.period.days != null && this.period.days.every(t => t.completed);
  }

  public setAllDays(completed: boolean) {
    this.period.completed = completed;
    if (this.period.days == null) {
      return;
    }
    this.period.days.forEach(t => t.completed = completed);
  }

  public defaultDevice(): string {
    if (this.formAction !== 'Crear' && this.rule.device.name) {
      return this.rule.device.name + " (" + this.rule.user.nickname + ")";
    } else {
      return 'Dispositivo'
    }
  }

  public defaultGroup(): string {
    if (this.formAction !== 'Crear'  && this.rule.group.name) {
      return this.rule.group.name + " (" + this.rule.user.nickname + ")";
    } else {
      return 'Grupo'
    }
  }

  private noticeUser(data): void {
    if (this.formAction === 'Crear') {
      this.notifyer.confirmSnackBar("La regla se ha creado con exito.");
    } else {
      this.notifyer.confirmSnackBar("La regla se ha actualizado con exito.");
    }
    this.rule.periodic ? this.router.navigate(['/dashboard/rules/periodic']) : this.router.navigate(['/dashboard/rules']);
  }

  private fillRule(): void {
    if (this.target == 'device') {
      this.rule.group = undefined;
    } else {
      this.rule.device = undefined;
    }

    if (this.rule.periodic) {
      this.backPath = '/dashboard/rules/periodic';
      this.rule.executeAt = undefined;
      this.rule.period = this.period.days.map(day => day.completed);
    } else {
      this.rule.initDate = undefined;
      this.rule.endDate = undefined;
    }
    var time = new Date();
    time.setHours(this.time.hour);
    time.setMinutes(this.time.minute);
    this.rule.time = time;
    this.rule.user = this.rule.device?.user || this.rule.group?.user;
  }

  private setRuleData(data): void {
    this.rule = data;

    if(this.rule.device){
      this.target = 'device';
      this.rule.group = new Group;
    }else{
      this.target = 'group';
      this.rule.device = new Device;
    }

    if (this.rule.periodic) {
      this.backPath = '/dashboard/rules/periodic';
    }
    this.time.hour = new Date(this.rule.time).getHours();
    this.time.minute = new Date(this.rule.time).getMinutes();
  }

  private passValidations(): boolean {
    if ((this.rule.device === undefined && this.rule.group === undefined) ||
      (this.rule.device === undefined && this.target === 'device') ||
      (this.rule.group === undefined && this.target === 'group')) {
      alert("Debe asignarle un dispositivo/grupo.");
      return false;
    }

    if (this.rule.periodic) {
      if (this.rule.initDate === undefined || this.rule.endDate === undefined) {
        alert("Debe seleccionar las fechas de inicio y de fin de periodo.");
        return false;
      }
      if (!this.someDay() && !this.period.completed) {
        alert("Debe seleccionar al menos un día de la semana.");
        return false;
      }
    } else {
      if (this.rule.executeAt === undefined) {
        alert("Debe seleccionar la fecha de ejecución.");
        return false;
      }
    }
    return true;
  }
}
