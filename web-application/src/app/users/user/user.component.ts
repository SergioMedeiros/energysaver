import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from "../../core/services/api.service";
import { NotifyerService } from "../../core/services/notifyer.service";
import { User } from '../../core/models/user.model';
import { StorageService } from "../../core/services/storage.service";

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  private userId: string;
  private old_password: string = '';
  public user: User = new User();
  public formAction: string = 'Actualizar';
  public backPath: string = '/dashboard';
  public currentUser: User = this.storageService.getCurrentUser();
  public updatePassword: boolean = false;
  public passwordTwo: string = '';

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private apiService: ApiService,
    private storageService: StorageService,
    private notifyer: NotifyerService
  ) {
    this.userId = this.route.snapshot.params.id;

    if (this.userId !== undefined) {
      this.apiService.getUser(this.userId).subscribe(
        data => this.user = data,
        error => console.log(error)
      )
    } else {
      this.formAction = 'Crear';
    }

    if (this.currentUser.role === 'admin') {
      this.backPath = '/dashboard/users';
    }
  }

  ngOnInit(): void {
    if (this.route.snapshot.params.id === undefined &&
      this.currentUser.role !== 'admin') {
      this.router.navigate(['/dashboard']);
    }
  }

  public createOrUpdateUser(): void {
    if (this.validateUser()) {
      if (this.formAction === 'Crear') {
        this.apiService.createUser(this.user).subscribe(
          data => this.noticeUser(data),
          error => alert(error.error)
        )
      } else {
        this.apiService.updateUser(this.user).subscribe(
          data => this.noticeUser(data),
          error => alert(error.error)
        )
      }
    }
  }

  public switchPasswordInputs(): void {
    if (this.updatePassword) {
      this.updatePassword = false;
      this.user.password = this.old_password;
    } else {
      this.updatePassword = true;
      this.old_password = this.user.password;
      this.user.password = '';
    }
  }

  private noticeUser(data): void {
    if (this.formAction === 'Crear') {
      this.notifyer.confirmSnackBar("El usuario se ha creado con exito.");
    } else {
      this.notifyer.confirmSnackBar("Los datos del usuraio se han actualizado.");
    }
    this.router.navigate(['/dashboard/users']);
  }

  private validateUser(): boolean {
    if (this.user.nickname === undefined ||
      this.user.nickname === '' ||
      this.user.email === undefined ||
      this.user.email === '') {
      alert("El usuario debe tener un nombre y un email asociado.");
      return false;
    } else if (this.formAction === 'Crear' || this.updatePassword === true) {
      if (this.user.password === undefined || this.user.password === '' || this.user.password !== this.passwordTwo) {
        alert("Ambas contraseñas deben ser iguales.");
        return false;
      }
    }
    return true;
  }
}
