import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from "../core/services/api.service";
import { NotifyerService } from "../core/services/notifyer.service";
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { StorageService } from "../core/services/storage.service";
import { Router } from '@angular/router';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  public displayedColumns: string[] = ['nickname', 'email', 'role', 'options'];
  public paginationOptions: number[] = [5, 10, 25, 50, 100];
  public dataSource: MatTableDataSource<Object>;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(
    private apiService: ApiService,
    private router: Router,
    private storageService: StorageService,
    private notifyer: NotifyerService
  ) {
    this.apiService.getUsers().subscribe(
      data => this.setUsersData(data),
      error => console.log(error)
    )
  }

  ngOnInit(): void {
    if (this.storageService.getCurrentUser().role !== 'admin'){
      this.router.navigate(['/dashboard']);
    }
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  public deleteUser(user: any): void {
    if (confirm("¿Seguro que quieres eliminar al usuario '" + user.nickname + "'?")) { //https://material.angular.io/components/dialog/examples
      this.apiService.deleteUser(user._id).subscribe(
        data => this.notifyAndReload(),
        error => console.log(error)
      )
    } else {
      this.notifyer.confirmSnackBar("El usuario no ha sido eliminado.");
    }
  }

  private setUsersData(data: Array<Object>): void {
    this.dataSource = new MatTableDataSource(data)
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  private notifyAndReload(): void {
    this.apiService.getUsers().subscribe(
      data => this.setUsersData(data),
      error => console.log(error)
    );
    this.notifyer.confirmSnackBar("El usuario ha sido eliminado.");
  }
}
