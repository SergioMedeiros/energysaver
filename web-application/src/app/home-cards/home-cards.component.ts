import { Component, OnInit } from '@angular/core';
import { StorageService } from "../core/services/storage.service";
import { User } from "../core/models/user.model";

@Component({
  selector: 'app-home-cards',
  templateUrl: './home-cards.component.html',
  styleUrls: ['./home-cards.component.css']
})
export class HomeCardsComponent implements OnInit {
  public user: User;
  public optionCards: Array<any> = [];
  public devicesCards: Array<any> = [];
  public cards: Array<any> = [];

  constructor(private storageService: StorageService) { }

  ngOnInit(): void {
    this.user = this.storageService.getCurrentUser();
    this.setAllOptions();
    this.setDevicesOptions();
    this.cards = this.optionCards;
  }

  private setAllOptions(): void {
    this.optionCards.push(
      {
        "imagePath": "../../assets/images/device-card.png",
        "title": "Dispositivos",
        "path": "devices"
      },
      {
        "imagePath": "../../assets/images/rule-card.png",
        "title": "Reglas",
        "path": "rules"
      },
      {
        "imagePath": "../../assets/images/user-card.png",
        "title": this.user.role === "admin" ? "Usuarios" : "Perfil",
        "path": this.user.role === "admin" ? "/dashboard/users" : "/dashboard/user/" + this.user.id
      }
    )
  }

  private setDevicesOptions(): void {
    this.devicesCards.push(
      {
        "imagePath": "../../assets/images/device-card.png",
        "imageName": "Dispositivos",
        "title": "Dispositivos",
        "path": "devices"
      },
      {
        "imagePath": "../../assets/images/rule-card.png",
        "imageName": "Reglas",
        "title": "Reglas",
        "path": "rules"
      },
      {
        "imagePath": "../../assets/images/user-card.png",
        "imageName": this.user.role === "admin" ? "Usuarios" : "Usuario",
        "title": this.user.role === "admin" ? "Usuarios" : "Perfil",
        "path": this.user.role === "admin" ? "/dashboard/users" : "/dashboard/user/" + this.user.id
      }
    )
  }
}
