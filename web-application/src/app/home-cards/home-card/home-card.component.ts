import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-home-card',
  templateUrl: './home-card.component.html',
  styleUrls: ['./home-card.component.css']
})
export class HomeCardComponent implements OnInit {
  @Input() imagePath: string = "../../assets/images/default-card.png";
  @Input() imageName: string = "Default";
  @Input() path: string = "dashboard";
  @Input() title: string = "Title";

  constructor() { }

  ngOnInit(): void {
  }
}
