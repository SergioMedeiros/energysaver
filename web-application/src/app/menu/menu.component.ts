import { Component, OnInit } from '@angular/core';
import { StorageService } from "../core/services/storage.service";
import { User } from "../core/models/user.model";
import { Router } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})

export class MenuComponent implements OnInit {
  public user: User;

  constructor(
    private storageService: StorageService,
    private router: Router
  ) { }

  ngOnInit() {
    this.user = this.storageService.getCurrentUser();
  }

  public logout(): void {
    this.storageService.logout();
  }

  public goToHome(): void {
    this.router.navigate(['/dashboard']);
  }
}
