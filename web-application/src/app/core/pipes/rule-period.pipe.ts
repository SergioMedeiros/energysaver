import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'period' })

export class rulePeriod implements PipeTransform {
    transform(period: boolean[]): string[] {
        var response: string[] = ['L', 'M', 'X', 'J', 'V', 'S', 'D'];
        
        var index = period.indexOf(false);
        while(index !== -1){
            response.splice(index, 1);
            period.splice(index, 1);
            index = period.indexOf(false);
        }
        return response;
    }
}