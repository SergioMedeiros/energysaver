import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { LoginObject } from "../models/login-object.model";
import { User } from "../models/user.model";

@Injectable({
    providedIn: "root"
})

export class AuthenticationService {
    private basePath: string = window.location.protocol + '//' + window.location.hostname + ':3000/api/';
    
    constructor(private http: HttpClient) { }

    signIn(loginObj: LoginObject): Observable<any>{
        return this.http.post(this.basePath + 'login', loginObj);
    }

    signUp(registerObj: User): Observable<any>{
        return this.http.post(this.basePath + 'register', registerObj);
    }
}