import { Injectable } from "@angular/core";
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
    providedIn: "root"
})

export class NotifyerService {
    constructor(
        private snackBar: MatSnackBar
    ) { }

    confirmSnackBar(message: string): void {
        this.snackBar.open(message, 'ok!', {
            duration: 5000,
            horizontalPosition: 'center',
            verticalPosition: 'top',
            panelClass: ['snackbar']
        });
    }
}