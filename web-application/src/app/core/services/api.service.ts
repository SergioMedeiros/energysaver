import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { StorageService } from "./storage.service";
import { Device } from '../models/device.model';
import { User } from '../models/user.model';
import { Rule } from '../models/rule.model';
import { Group } from '../models/group.model';

@Injectable({
    providedIn: "root"
})

export class ApiService {
    private basePath: string = window.location.protocol + '//' + window.location.hostname + ':3000/api/';
    private headers: HttpHeaders = new HttpHeaders().set("Content-Type", "application/json").set("Authorization", this.storageService.getCurrentToken());

    constructor(
        private http: HttpClient,
        private storageService: StorageService) { }

    // DEVICES

    getDevices(): Observable<any> {
        return this.http.get(this.basePath + 'devices', { headers: this.headers });
    }

    getDevicesFree(groupId: string): Observable<any> {
        return this.http.get(this.basePath + 'groups/' + groupId + '/devices', { headers: this.headers });
    }

    getDevicesFreeFor(groupId: string, userId: string): Observable<any> {
        return this.http.get(this.basePath + 'groups/' + groupId + '/devices/' + userId, { headers: this.headers });
    }

    getDevice(id: string): Observable<any> {
        return this.http.get(this.basePath + 'devices/' + id, { headers: this.headers });
    }

    getDevicesFor(id: string): Observable<any> {
        return this.http.get(this.basePath + 'users/' + id + '/devices', { headers: this.headers });
    }

    deleteDevice(id: string): Observable<any> {
        return this.http.delete(this.basePath + 'devices/' + id, { headers: this.headers });
    }

    createDevice(device: Device): Observable<any> {
        return this.http.post(this.basePath + 'devices', this.newDevice(device), { headers: this.headers });
    }

    updateDevice(device: Device): Observable<any> {
        return this.http.put(this.basePath + 'devices/' + device._id, device, { headers: this.headers });
    }

    addDeviceGroup(deviceId: string, groupId: string): Observable<any> {
        return this.http.put(this.basePath + 'devices/' + deviceId + '/group', { group: groupId }, { headers: this.headers });
    }

    freeDeviceGroup(deviceId: string, groupId: string): Observable<any> {
        return this.http.put(this.basePath + 'devices/' + deviceId + '/free', { group: groupId }, { headers: this.headers });
    }

    // USERS

    getUsers(): Observable<any> {
        return this.http.get(this.basePath + 'users', { headers: this.headers });
    }

    deleteUser(id: string): Observable<any> {
        return this.http.delete(this.basePath + 'users/' + id, { headers: this.headers });
    }

    getUser(id: string): Observable<any> {
        return this.http.get(this.basePath + 'users/' + id, { headers: this.headers });
    }

    createUser(user: User): Observable<any> {
        return this.http.post(this.basePath + 'users', user, { headers: this.headers });
    }

    updateUser(user: User): Observable<any> {
        return this.http.put(this.basePath + 'users/' + user._id, user, { headers: this.headers });
    }

    // RULES

    getRules(): Observable<any> {
        return this.http.get(this.basePath + 'rules', { headers: this.headers });
    }

    getPunctualRules(): Observable<any> {
        return this.http.get(this.basePath + 'rules/punctual', { headers: this.headers });
    }

    getPeriodicRules(): Observable<any> {
        return this.http.get(this.basePath + 'rules/periodic', { headers: this.headers });
    }

    getRule(id: string): Observable<any> {
        return this.http.get(this.basePath + 'rules/' + id, { headers: this.headers });
    }

    getRulesForUser(id: string): Observable<any> {
        return this.http.get(this.basePath + 'users/' + id + '/rules', { headers: this.headers });
    }

    getPunctualRulesForUser(id: string): Observable<any> {
        return this.http.get(this.basePath + 'users/' + id + '/rules/punctual', { headers: this.headers });
    }

    getPeriodicRulesForUser(id: string): Observable<any> {
        return this.http.get(this.basePath + 'users/' + id + '/rules/periodic', { headers: this.headers });
    }

    getRulesForDevice(id: string): Observable<any> {
        return this.http.get(this.basePath + 'devices/' + id + '/rules', { headers: this.headers });
    }

    getPunctualRulesForDevice(id: string): Observable<any> {
        return this.http.get(this.basePath + 'devices/' + id + '/rules/punctual', { headers: this.headers });
    }

    getPeriodicRulesForDevice(id: string): Observable<any> {
        return this.http.get(this.basePath + 'devices/' + id + '/rules/periodic', { headers: this.headers });
    }

    createRule(rule: Rule): Observable<any> {
        return this.http.post(this.basePath + 'rules', rule, { headers: this.headers });
    }

    updateRule(rule: Rule): Observable<any> {
        return this.http.put(this.basePath + 'rules/' + rule._id, rule, { headers: this.headers });
    }

    enableRule(rule: Rule): Observable<any> {
        return this.http.put(this.basePath + 'rules/' + rule._id + '/enable', {}, { headers: this.headers });
    }

    disableRule(rule: Rule): Observable<any> {
        return this.http.put(this.basePath + 'rules/' + rule._id + '/disable', {}, { headers: this.headers });
    }

    deleteRule(id: string): Observable<any> {
        return this.http.delete(this.basePath + 'rules/' + id, { headers: this.headers });
    }

    // GROUPS

    getGroups(): Observable<any> {
        return this.http.get(this.basePath + 'groups', { headers: this.headers });
    }

    getGroup(id: string): Observable<any> {
        return this.http.get(this.basePath + 'groups/' + id, { headers: this.headers });
    }

    getGroupsFor(id: string): Observable<any> {
        return this.http.get(this.basePath + 'users/' + id + '/groups', { headers: this.headers });
    }

    deleteGroup(id: string): Observable<any> {
        return this.http.delete(this.basePath + 'groups/' + id, { headers: this.headers });
    }

    createGroup(group: Group): Observable<any> {
        return this.http.post(this.basePath + 'groups', group, { headers: this.headers });
    }

    updateGroup(group: Group): Observable<any> {
        return this.http.put(this.basePath + 'groups/' + group._id, this.normalizeGroup(group), { headers: this.headers });
    }
    
    freeGroupDevice(groupId: string, deviceId: string): Observable<any> {
        return this.http.put(this.basePath + 'groups/' + groupId + '/free', { device: deviceId }, { headers: this.headers });
    }

    // PRIVATE 

    private newDevice(device: Device): any {
        var newDevice = {};
        newDevice['name'] = device.name;
        newDevice['description'] = device.description;
        newDevice['type'] = device.type;
        newDevice['os'] = device.os;
        newDevice['mac'] = device.mac;
        newDevice['user'] = device.user._id || device.user.id;
        return newDevice;
    }

    private normalizeGroup(group: Group): any {
        var newGroup = {};
        newGroup['name'] = group.name;
        newGroup['description'] = group.description;
        newGroup['user'] = group.user._id;
        newGroup['devices'] = group.devices.map(device => device._id);
        return newGroup;
    }
}