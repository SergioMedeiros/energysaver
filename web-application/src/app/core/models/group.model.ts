import { User } from './user.model';
import { Device } from './device.model';

export class Group {
    public _id: string;
    public name: string;
    public description: string;
    public devices: Device[];
    public user: User;

    constructor() {
        this.description = ' ';
        this.devices = [];
    }
}