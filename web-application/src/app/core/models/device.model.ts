import { User } from './user.model';
import { Group } from './group.model';

export class Device {
    public _id: string;
    public name: string;
    public description: string;
    public type: string;
    public mac: string;
    public os: string;
    public groups: Array<Group>;
    public user: User;

    constructor() {
        this.description = ' ';
        this.type = 'workstation';
        this.os = 'windows';
        this.groups = [];
    }
}