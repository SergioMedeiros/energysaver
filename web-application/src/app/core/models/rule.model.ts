import { User } from './user.model';
import { Device } from './device.model';
import { Group } from './group.model';

export class Rule {
    public _id: string;
    public type: string;        // on / off
    public time: Date;
    public executeAt: Date;
    public active: boolean;
    public periodic: boolean;
    public period: boolean[];
    public initDate: Date;
    public endDate: Date;
    public command: string;
    public device: Device;
    public group: Group;
    public user: User;

    constructor() {
        this.type = 'on';       // on / off
        this.active = true;
        this.periodic = false;
        this.period = [];
        this.time = new Date();
        this.device = null;
        this.group = null;
    }
}