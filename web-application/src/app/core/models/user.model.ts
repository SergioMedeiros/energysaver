export class User {
    public id: string;
    public _id: string;
    public nickname: string;
    public role: string;
    public email: string;
    public password: string;

    constructor() {
        this.role = 'developer';
    }
}