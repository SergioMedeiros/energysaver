// Modules
import { NgModule, Optional, SkipSelf } from '@angular/core';

//Services
import { StorageService } from "./services/storage.service";
import { AuthenticationService } from "./services/authentication.service";

//Guards
import { AuthorizatedGuard } from "./guards/authorizated.guard";

@NgModule({
    declarations: [],
    imports: [],
    providers: [
        StorageService,
        AuthenticationService,
        AuthorizatedGuard
    ],
    bootstrap: []
})

export class CoreModule {
    constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
        if (parentModule) {
            throw new Error(
                'CoreModule is already loaded. Import it in the AppModule only');
        }
    }
}
