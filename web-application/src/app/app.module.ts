// Modules
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from "@angular/common/http";
import { AppRoutingModule } from './app-routing.module';
import { CoreModule } from "./core/core.module";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomeModule } from './home/home.module';
import { MatNativeDateModule } from '@angular/material/core';

// Components
import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ], // Components
  imports: [
    BrowserModule,
    HomeModule,
    HttpClientModule,
    AppRoutingModule,
    CoreModule,
    BrowserAnimationsModule,
    MatNativeDateModule
  ], // Modules here
  providers: [], // services here
  bootstrap: [AppComponent]
})

export class AppModule { }
