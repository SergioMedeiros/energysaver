// Modules
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Components
import { HomeComponent } from "./home/home.component";
import { LoginComponent } from "./login/login.component";
import { HomeCardsComponent } from "./home-cards/home-cards.component";
import { DevicesComponent } from './devices/devices.component';
import { DeviceComponent } from './devices/device/device.component';
import { RulesComponent } from './rules/rules.component';
import { RuleComponent } from './rules/rule/rule.component';
import { UsersComponent } from './users/users.component';
import { UserComponent } from './users/user/user.component';
import { GroupsComponent } from './groups/groups.component';
import { GroupComponent } from './groups/group/group.component';

// Guards
import { AuthorizatedGuard } from "./core/guards/authorizated.guard";

const routes: Routes = [
  {
    path: 'dashboard', component: HomeComponent, canActivate: [AuthorizatedGuard], children: [
      { path: 'device/:id', component: DeviceComponent },
      { path: 'device', component: DeviceComponent },
      { path: 'devices', component: DevicesComponent },
      { path: 'rule/:id', component: RuleComponent },
      { path: 'rule', component: RuleComponent },
      { path: 'rules', component: RulesComponent },
      { path: 'rules/:type', component: RulesComponent },
      { path: 'user/:id', component: UserComponent },
      { path: 'user', component: UserComponent },
      { path: 'users', component: UsersComponent },
      { path: 'group/:id', component: GroupComponent },
      { path: 'group', component: GroupComponent },
      { path: 'groups', component: GroupsComponent },
      { path: '', component: HomeCardsComponent }
    ]
  },
  { path: 'login', component: LoginComponent },
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: '**', redirectTo: '/dashboard' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
