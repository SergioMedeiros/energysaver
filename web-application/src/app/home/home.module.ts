import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTableModule } from '@angular/material/table';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatToolbarModule } from '@angular/material/toolbar';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { HomeComponent } from './home.component';
import { LoginComponent } from '../login/login.component';
import { MenuComponent } from '../menu/menu.component';
import { FooterComponent } from '../footer/footer.component';
import { HomeCardsComponent } from '../home-cards/home-cards.component';
import { HomeCardComponent } from '../home-cards/home-card/home-card.component';
import { DevicesComponent } from '../devices/devices.component';
import { DeviceComponent } from '../devices/device/device.component';
import { RulesComponent } from '../rules/rules.component';
import { RuleComponent } from '../rules/rule/rule.component';
import { UsersComponent } from '../users/users.component';
import { UserComponent } from '../users/user/user.component';
import { GroupsComponent } from '../groups/groups.component';
import { GroupComponent } from '../groups/group/group.component';

import { rulePeriod } from '../core/pipes/rule-period.pipe';
import { ellipsis } from '../core/pipes/ellipsis.pipe';

@NgModule({
  declarations: [
    HomeComponent,
    LoginComponent,
    MenuComponent,
    FooterComponent,
    HomeCardsComponent,
    HomeCardComponent,
    DevicesComponent,
    DeviceComponent,
    RulesComponent,
    GroupsComponent,
    GroupComponent,
    RuleComponent,
    UsersComponent,
    UserComponent,
    rulePeriod,
    ellipsis
  ],
  imports: [
    CommonModule,
    FormsModule,
    NgbModule,
    ReactiveFormsModule,
    MatCardModule,
    DragDropModule,
    MatButtonModule,
    MatInputModule,
    MatMenuModule,
    MatIconModule,
    MatSelectModule,
    MatSnackBarModule,
    MatTableModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatSlideToggleModule,
    MatPaginatorModule,
    MatSortModule,
    MatToolbarModule,
    RouterModule
  ]
})

export class HomeModule { }
