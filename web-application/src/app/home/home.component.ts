import { Component, OnInit } from '@angular/core';
import { StorageService } from "../core/services/storage.service";
import { User } from "../core/models/user.model";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {
  public user: User;

  constructor(
    private storageService: StorageService
  ) { }

  ngOnInit() {
    this.user = this.storageService.getCurrentUser();
  }
}
