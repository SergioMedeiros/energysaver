import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from "../core/services/api.service";
import { StorageService } from "../core/services/storage.service";
import { NotifyerService } from "../core/services/notifyer.service";
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Device } from '../core/models/device.model';

@Component({
  selector: 'app-devices',
  templateUrl: './devices.component.html',
  styleUrls: ['./devices.component.css']
})
export class DevicesComponent implements OnInit {
  public displayedColumns: string[] = ['name', 'description', 'mac', 'type', 'os', 'user', 'options'];
  public paginationOptions: number[] = [5, 10, 25, 50, 100];
  public dataSource: MatTableDataSource<Object>;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(
    private apiService: ApiService,
    private storageService: StorageService,
    private notifyer: NotifyerService
  ) {
    this.loadDevices();
  }

  ngOnInit(): void { }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  public deleteDevice(device: any): void {
    if (confirm("¿Seguro que quieres eliminar el dispositivo '" + device.name + "'?")) { //https://material.angular.io/components/dialog/examples
      this.apiService.deleteDevice(device._id).subscribe(
        data => this.freeGroup(data),
        error => console.log(error)
      )
    } else {
      this.notifyer.confirmSnackBar("El dispositivo no ha sido eliminado.");
    }
  }

  private setDevicesData(data: Array<Device>): void {
    this.dataSource = new MatTableDataSource(this.normalizeData(data));
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  private normalizeData(data: Array<Device>): Array<any> {
    return data.map(function (device) {
      return {
        _id: device._id,
        mac: device.mac,
        name: device.name,
        description: device.description,
        type: device.type,
        os: device.os,
        user: device.user.nickname
      }
    })
  }

  private freeGroup(data): void {
    console.log(data);
    data.groups.forEach(group => {
      this.apiService.freeGroupDevice(group, data._id).subscribe()
    });

    this.notifyAndReload();
  }

  private notifyAndReload(): void {
    this.notifyer.confirmSnackBar("El dispositivo ha sido eliminado.");
    this.loadDevices();
  }

  private loadDevices(): void {
    var user = this.storageService.getCurrentUser();

    if (user.role === "admin") {
      this.apiService.getDevices().subscribe(
        data => this.setDevicesData(data),
        error => console.log(error)
      )
    } else {
      this.apiService.getDevicesFor(user.id).subscribe(
        data => this.setDevicesData(data),
        error => console.log(error)
      )
    }
  }
}
