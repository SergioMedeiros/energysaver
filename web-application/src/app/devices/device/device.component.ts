import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from "../../core/services/api.service";
import { NotifyerService } from "../../core/services/notifyer.service";
import { Device } from '../../core/models/device.model';
import { User } from '../../core/models/user.model';
import { StorageService } from "../../core/services/storage.service";

@Component({
  selector: 'app-device',
  templateUrl: './device.component.html',
  styleUrls: ['./device.component.css']
})
export class DeviceComponent implements OnInit {
  private deviceId: string;
  public device: Device = new Device();
  public formAction: string = 'Actualizar';
  public updateOwner: boolean = false;
  public currentUser: User;
  public users: Array<User>;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private apiService: ApiService,
    private storageService: StorageService,
    private notifyer: NotifyerService
  ) {
    this.deviceId = this.route.snapshot.params.id;
    this.currentUser = this.storageService.getCurrentUser();

    if (this.deviceId !== undefined) {
      this.apiService.getDevice(this.deviceId).subscribe(
        data => this.device = data,
        error => console.log(error)
      )
    } else {
      this.formAction = 'Crear';
      this.device.user = this.currentUser;
    }

    if (this.currentUser.role === 'admin') {
      this.apiService.getUsers().subscribe(
        data => this.users = data,
        error => console.log(error)
      )
    }
  }

  ngOnInit(): void {
  }

  public createOrUpdateDevice(): void {
    if (this.device.name === undefined || this.device.mac === undefined || this.device.name === '' || this.device.mac === '') {
      alert("Un dispositivo debe tener al menos un nombre y una dirección MAC.");
    } else {
      if (this.formAction === 'Crear') {
        this.apiService.createDevice(this.device).subscribe(
          data => this.noticeUser(data),
          error => alert(error.error)
        )
      } else {
        this.apiService.updateDevice(this.device).subscribe(
          data => this.noticeUser(data),
          error => alert(error.error)
        )
      }
    }
  }

  public switchOwnerSelector(): void {
    this.updateOwner = !this.updateOwner;
  }

  private noticeUser(data): void {
    if (this.formAction === 'Crear') {
      this.notifyer.confirmSnackBar("El dispositivo se ha creado con exito.");
    } else {
      this.notifyer.confirmSnackBar("El dispositivo se ha actualizado con exito.");
    }
    this.router.navigate(['/dashboard/devices']);
  }
}
