import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from "../../core/services/api.service";
import { NotifyerService } from "../../core/services/notifyer.service";
import { Device } from '../../core/models/device.model';
import { Group } from '../../core/models/group.model';
import { User } from '../../core/models/user.model';
import { StorageService } from "../../core/services/storage.service";
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-group',
  templateUrl: './group.component.html',
  styleUrls: ['./group.component.css']
})
export class GroupComponent implements OnInit {
  private groupId: string;
  public group: Group = new Group();
  public formAction: string = 'Actualizar';
  public currentUser: User;
  public freeDevices: Array<Device>;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private apiService: ApiService,
    private storageService: StorageService,
    private notifyer: NotifyerService
  ) {
    this.groupId = this.route.snapshot.params.id;
    this.currentUser = this.storageService.getCurrentUser();

    if (this.groupId !== undefined) {
      this.apiService.getGroup(this.groupId).subscribe(
        data => this.loadDevices(data),
        error => console.log(error)
      )
    } else {
      this.formAction = 'Crear';
      this.group.user = this.currentUser;
      this.group.user._id = this.currentUser.id;

      if (this.currentUser.role === "admin") {
        this.apiService.getDevices().subscribe(
          data => this.freeDevices = data,
          error => console.log(error)
        )
      } else {
        this.apiService.getDevicesFor(this.currentUser.id).subscribe(
          data => this.freeDevices = data,
          error => console.log(error)
        )
      }
    }
  }

  ngOnInit(): void {
  }

  public drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
    }
  }

  public createOrUpdateGroup(): void {
    if (this.group.name === undefined || this.group.name === '') {
      alert("Debe asignar un nombre a este grupo.");
    } else if (this.group.devices.length === 0) {
      alert("Debe asignar al menos un dispositivo a este grupo.");
    } else {
      if (this.formAction === 'Crear') {
        this.apiService.createGroup(this.group).subscribe(
          data => this.updateDevices(data),
          error => alert(error.error)
        )
      } else {
        this.apiService.updateGroup(this.group).subscribe(
          data => this.updateDevices(data),
          error => alert(error.error)
        )
      }
    }
  }

  private updateDevices(data): void {
    this.updateSelectedDevices(data);
    this.updateFreeDevices(data);
    this.noticeUser();
  }

  private updateSelectedDevices(data): void {
    this.group.devices.forEach(device => this.apiService.addDeviceGroup(device._id, data._id).subscribe());
  }

  private updateFreeDevices(data): void {
    this.freeDevices.forEach(device => {
      if (device.groups.includes(data._id)) {
        this.apiService.freeDeviceGroup(device._id, data._id).subscribe()
      }
    });
  }

  private noticeUser(): void {
    if (this.formAction === 'Crear') {
      this.notifyer.confirmSnackBar("El grupo se ha creado con exito.");
    } else {
      this.notifyer.confirmSnackBar("El grupo se ha actualizado con exito.");
    }
    this.router.navigate(['/dashboard/groups']);
  }

  private loadDevices(data): void {
    this.group = data;
    
    if (this.currentUser.role === "admin" && this.currentUser.id === this.group.user._id) {
      this.apiService.getDevicesFree(this.group._id).subscribe(
        data => this.freeDevices = data,
        error => console.log(error)
      );
    } else {
      this.apiService.getDevicesFreeFor(this.group._id, this.group.user._id).subscribe(
        data => this.freeDevices = data,
        error => console.log(error)
      );
    }
  }
}
