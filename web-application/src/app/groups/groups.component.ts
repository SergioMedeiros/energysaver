import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from "../core/services/api.service";
import { StorageService } from "../core/services/storage.service";
import { NotifyerService } from "../core/services/notifyer.service";
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Device } from '../core/models/device.model';

@Component({
  selector: 'app-groups',
  templateUrl: './groups.component.html',
  styleUrls: ['./groups.component.css']
})
export class GroupsComponent implements OnInit {
  public displayedColumns: string[] = ['name', 'description', 'devices', 'user', 'options'];
  public paginationOptions: number[] = [5, 10, 25, 50, 100];
  public dataSource: MatTableDataSource<Object>;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(
    private apiService: ApiService,
    private storageService: StorageService,
    private notifyer: NotifyerService
  ) {
    this.loadGroups();
  }

  ngOnInit(): void { }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  public deleteGroup(group: any): void {
    if (confirm("¿Seguro que quieres eliminar el grupo '" + group.name + "'?")) {
      this.apiService.deleteGroup(group._id).subscribe(
        data => this.freeDevices(data),
        error => console.log(error)
      )
    } else {
      this.notifyer.confirmSnackBar("El grupo no ha sido eliminado.");
    }
  }

  private freeDevices(data): void {
    data.devices.forEach(device => {
      this.apiService.freeDeviceGroup(device, data._id).subscribe()
    });

    this.notifyAndReload();
  }

  private notifyAndReload(): void {
    this.notifyer.confirmSnackBar("El grupo ha sido eliminado.");
    this.loadGroups();
  }

  private loadGroups(): void {
    var user = this.storageService.getCurrentUser();

    if (user.role === "admin") {
      this.apiService.getGroups().subscribe(
        data => this.setGroupsData(data),
        error => console.log(error)
      )
    } else {
      this.apiService.getGroupsFor(user.id).subscribe(
        data => this.setGroupsData(data),
        error => console.log(error)
      )
    }
  }

  private setGroupsData(data: Array<Device>): void {
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
}
