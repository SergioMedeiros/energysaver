#
#Copyright (C) 2020  Sergio Medeiros García
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>
#

# Installing the package
echo "Installing apache..."
sudo yum install -y httpd
echo "Apache installed!"

# Replace httpd.conf (configuration file of apache)
sudo cp ./server-configuration/httpd-files/httpd.conf /etc/httpd/conf/

# Build web application and move it into /var/www/energysaver
echo "Deploying web..."
sudo mkdir /var/www/energysaver
sudo mkdir /var/www/energysaver/logs
sudo mkdir /var/www/energysaver/public_html
cd ./web-application/
echo n | npm install
ng build
sudo cp -r ./dist/web-application/* /var/www/energysaver/public_html
echo "Web deployed!"

# Change hostname
sudo hostnamectl set-hostname energysaver.com

# Firewall configuration - ports 80 & 443
sudo firewall-cmd --permanent --zone=public --add-service=http
sudo firewall-cmd --permanent --zone=public --add-service=https
sudo firewall-cmd --reload

# Start httpd
sudo systemctl enable httpd --now
echo "Apache is ready!!"
