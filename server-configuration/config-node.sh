#
#Copyright (C) 2020  Sergio Medeiros García
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>
#

# Install previous package
echo "Installing NodeJS..."
sudo yum groupinstall -y 'Development Tools'

# Set node repo
sudo curl -sL https://rpm.nodesource.com/setup_12.x | bash -

# Install NodeJS
sudo yum install -y nodejs
echo "NodeJS installed!"

# Install angular-cli
echo "Installing angular-cli..."
echo n | sudo npm install -g @angular/cli
echo "angular-cli installed!"

# Copy api-rest folder into our home folder
echo "Installing API..."
cp -r api-rest/ /home/$(whoami)
echo "Installing Runner..."
cp -r server-configuration/runner/ /home/$(whoami)
cd /home/$(whoami)/api-rest
npm install --yes
node api/index.js &
echo "API installed!"
cd ../runner
mkdir log
npm install --yes
echo "Runner installed!"

# Firewall configuration -  port 3000
sudo firewall-cmd --permanent --zone=public --add-port=3000/tcp
sudo firewall-cmd --reload