#
#Copyright (C) 2020  Sergio Medeiros García
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>
#

# get current network configuration
device="$(nmcli con | grep ethernet | cut -d' ' -f1)"
ip="$(nmcli device show $device | grep IP4.ADDRESS | awk '{print $2}')"
gateway="$(nmcli device show $device | grep IP4.GATEWAY | awk '{print $2}')"
dns_1="$(nmcli device show $device | grep 'IP4.DNS\[1\]' | awk '{print $2}')"
dns_2="$(nmcli device show $device | grep 'IP4.DNS\[2\]' | awk '{print $2}')"

# create new network connection
echo "creating new network connection with static ip..."
nmcli con add con-name "static-$device" ifname $device type ethernet ip4 $ip gw4 $gateway
nmcli con mod "static-$device" ipv4.dns "$dns_1,$dns_2"
nmcli con up "static-$device" iface $device
echo "connected with static ip!"

# delete default connection
nmcli con del $device
