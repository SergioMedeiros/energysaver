/*
Copyright (C) 2020  Sergio Medeiros García

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*/
"use strict";

// Loading modules
const sys = require('utils');
const exec = require('child_process').exec;
const request = require('request');

function updateRule(rule, time) {
    if (rule.periodic && !(new Date(rule.endDate).getDate() === time.getDate())) {
        return false;
    }

    request({ url: 'http://localhost:3000/api/rules/' + rule._id + '/finish', method: 'PUT', json: {} }, (err, res) => {
        exec("echo '[" + time.toLocaleString() + "] - Rule " + rule._id + " has been disabled.' >> ~/runner/log/runner.log");
    });
}

function wakeUpDevice(rule, device, time) {
    if (rule.periodic && rule.period[time.getDay()]) {
        return false;
    }
    exec("echo '[" + time.toLocaleString() + "] - Time to wake up " + device.name + ".' >> ~/runner/log/runner.log");
    exec("wol " + device.mac + " >> ~/runner/log/runner.log");
    return true;
}

request('http://localhost:3000/api/rules/to-turn-on', { json: true }, (err, res) => {
    if (err) { return console.log(err); }

    var now = new Date();
    res.body.forEach(function (rule) {
        var ruleTime = new Date(rule.time);
        if (ruleTime.getHours() == now.getHours() &&
            (now.getMinutes() + 1) >= ruleTime.getMinutes()) {
            if (rule.device) {
                wakeUpDevice(rule, rule.device, now);
                updateRule(rule, now);
            } else {
                rule.group.devices.forEach(device => {
                    wakeUpDevice(rule, device, now);
                });
                updateRule(rule, now);
            }
        }
    });
});

