#
#Copyright (C) 2020  Sergio Medeiros García
#
#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation, either version 3 of the License, or
#(at your option) any later version.
#
#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.
#
#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>
#

# Adding the mongo repository by copying the file mongodb.repo inside the directory /etc/yum.repos.d/
echo "Adding mongo's repository..."
sudo cp ./server-configuration/mongo-files/mongodb-org.repo /etc/yum.repos.d/

# Installing mongodb
echo "Installing mongo..."
sudo dnf install -y mongodb-org
echo "Mongo installed!"

# Some configuration to secure mongod
sudo cp ./server-configuration/mongo-files/mongod.conf /etc/

# Configuring mongodb daemon
sudo systemctl enable mongod --now

# Create an admin in mongo
mongo < ./server-configuration/mongo-files/create-admin.js

# Create energysaver user in energysaver DB
mongo admin -u admin -p admin < ./server-configuration/mongo-files/create-energysaver-db.js

# Create energysaver user in energysaver DB
mongo energysaver -u energysaver -p energysaver < ./server-configuration/mongo-files/create-energysaver-admin.js

echo "Mongo configured!"
