db.users.insert(
    {
        "nickname":     "admin",
        "email":        "admin@energysaver.com",
        "password":     "$2b$14$otU5ssb.jH8rf..9n2n9nuB7V2ZtbYO7XGQLfnbatoeRR1Fa.bNcm",
        "role":         "admin"
    }
)