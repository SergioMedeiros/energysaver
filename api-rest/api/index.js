/*
Copyright (C) 2020  Sergio Medeiros García

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*/
"use strict";

// Loading mongoose to connect with mongo
var mongoose = require('mongoose');

// Loading our express app with our routes
var app = require('./app');

// Connecting with mongo
mongoose.connect('mongodb://energysaver:energysaver@localhost:27017/energysaver', { useNewUrlParser: true, useUnifiedTopology: true }, function(err, res) {
  if(err) {
    console.log('ERROR: connecting to Database: ' + err);
  }
  // Starting API
  app.listen(3000, function() {
    console.log("Node server running!!");
  });
});
