/*
Copyright (C) 2020  Sergio Medeiros García

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*/
"use strict";

var Device = require("../models/device");
var User = require("../models/user");
var Rule = require("../models/rule");

//GET - Return all devices in the DB
exports.findAllDevices = function (req, res) {
    Device.find(function (err, devices) {
        if (err) return res.status(500).send(err.message);
        User.populate(devices, { path: "user" }, function (err, devices) {
            if (err) return res.status(500).send(err.message);
            res.status(200).jsonp(devices);
        });
    });
}

//GET - Return a device with specified ID
exports.findDeviceByID = function (req, res) {
    Device.findById(req.params.id, function (err, device) {
        if (err) return res.status(500).send(err.message);
        User.populate(device, { path: "user" }, function (err, device) {
            if (err) return res.status(500).send(err.message);
            res.status(200).jsonp(device);
        });
    });
};

//GET - Return all devices of a specific User
exports.findUserDevices = function (req, res) {
    Device.find({ user: req.params.id }, function (err, device) {
        if (err) return res.status(500).send(err.message);
        User.populate(device, { path: "user" }, function (err, device) {
            if (err) return res.status(500).send(err.message);
            res.status(200).jsonp(device);
        });
    });
};

//GET - Return all devices not included in a group
exports.findFreeDevices = function (req, res) {
    Device.find({ groups: { $nin: [req.params.id] } }, function (err, device) {
        if (err) return res.status(500).send(err.message);
        User.populate(device, { path: "user" }, function (err, device) {
            if (err) return res.status(500).send(err.message);
            res.status(200).jsonp(device);
        });
    });
};

//GET - Return all devices not included in a group
exports.findFreeDevicesFor = function (req, res) {
    Device.find({ $and: [{ user: req.params.user }, { groups: { $nin: [req.params.id] } }] }, function (err, device) {
        if (err) return res.status(500).send(err.message);
        User.populate(device, { path: "user" }, function (err, device) {
            if (err) return res.status(500).send(err.message);
            res.status(200).jsonp(device);
        });
    });
};

//POST - Insert a new device in the DB
exports.createDevice = function (req, res) {
    Device.find({ mac: req.body.mac }, function (err, device) {
        if (err) return res.status(500).send(err.message);

        if (!device[0]) { // Device does not exist
            var device = new Device({
                name: req.body.name,
                description: req.body.description,
                type: req.body.type,
                mac: req.body.mac,
                os: req.body.os,
                groups: req.body.groups,
                user: req.body.user
            });

            device.save(function (err, device) {
                if (err) return res.status(500).send(err.message);
                res.status(201).jsonp(device);
            });
        } else { // Device already exist
            res.status(500).send("Este dispositivo ya existe.");
        }
    });
};

//PUT - Update a device that already exists
exports.updateDevice = function (req, res) {
    Device.findById(req.params.id, function (err, device) {
        if (err) return res.status(500).send("Dispositivo no encontrado.");

        if (device.mac === req.body.mac) {
            device.name = req.body.name;
            device.description = req.body.description;
            device.type = req.body.type;
            device.os = req.body.os;
            device.groups = req.body.groups;
            device.user = req.body.user;

            device.save(function (err) {
                if (err) res.status(500).send(err.message);
                res.status(200).jsonp(device);
            });
        } else {
            Device.find({ mac: req.body.mac }, function (err, another_device) {
                if (err) return res.send(500, err.message);

                if (another_device[0]) { // Another device exists with this mac address
                    return res.status(500).send("Esta direccion MAC ya existe.");
                } else {
                    device.name = req.body.name;
                    device.description = req.body.description;
                    device.type = req.body.type;
                    device.os = req.body.os;
                    device.groups = req.body.groups;
                    device.mac = req.body.mac;
                    device.user = req.body.user;

                    device.save(function (err) {
                        if (err) res.status(500).send(err.message);
                        res.status(200).jsonp(device);
                    });
                }
            });
        }
    });
};

//PUT - Include a device in a group
exports.addDeviceGroup = function (req, res) {
    Device.findById(req.params.id, function (err, device) {
        if (err) return res.status(500).send("Dispositivo no encontrado.");

        var index = device.groups.indexOf(req.body.group);
        if (index < 0) {
            device.groups.push(req.body.group);
        }

        device.save(function (err) {
            if (err) res.status(500).send(err.message);
            res.status(200).jsonp(device);
        });
    });
};

//PUT - Exclude a device of a group
exports.freeDeviceGroup = function (req, res) {
    Device.findById(req.params.id, function (err, device) {
        if (err) return res.status(500).send("Dispositivo no encontrado.");

        var index = device.groups.indexOf(req.body.group);
        if (index > -1) {
            device.groups.splice(index, 1);
        }

        device.save(function (err) {
            if (err) res.status(500).send(err.message);
            res.status(200).jsonp(device);
        });
    });
};

//DELETE - Delete a device with specified ID
exports.deleteDevice = function (req, res) {
    Device.findById(req.params.id, function (err, device) {
        if (err) return res.status(500).send(err.message);
        if (!device) return res.status(500).send("Dispositivo no encontrado.");

        device.remove(function (err) {
            if (err) return res.status(500).send(err.message);
            Rule.find({ device: device._id }, function (err, rules) {
                if (err) return res.status(500).send(err.message);
                rules.forEach(rule => rule.remove(function (err) {
                    if (err) return res.status(500).send(err.message);
                }))
            });
            return res.status(200).jsonp(device);
        })
    });
};