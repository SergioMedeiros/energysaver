/*
Copyright (C) 2020  Sergio Medeiros García

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*/
"use strict";

var bcrypt = require('bcrypt');
var config = require("../config/config");
var User  = require("../models/user");
var Device  = require("../models/device");
var Rule  = require("../models/rule");
    
//GET - Return all users in the DB
exports.findAllUsers = function(req, res) {
    User.find(function(err, users) {
        if(err) return res.status(500).send(err.message);

        res.status(200).jsonp(users);
    });
}

//GET - Return a user with specified ID
exports.findUserByID = function(req, res) {
    User.findById(req.params.id, function(err, user) {
        if(err) return res.status(500).send("Usuario no encontrado.");

        res.status(200).jsonp(user);
    });
};

//POST - Insert a new User in the DB
exports.createUser = function(req, res) {
    User.find({email: req.body.email}, function(err, user) {
        if(err) return res.status(500).send(err.message);

        if(!user[0]){ // If user does not exist
            bcrypt.hash(req.body.password, config.salt)
            .then(function(password) {
                var user = new User({
                    nickname: req.body.nickname,
                    email: req.body.email,
                    password: password,
                    role: req.body.role
                });
                
                user.save(function(err, user) {
                    if(err) return res.status(500).send(err.message);
                    res.status(201).jsonp(user);
                });
            });
        }else{ // User already exists
            res.status(500).send("Este usuario ya existe.");
        }
    });
};

//PUT - Update a user that already exists
exports.updateUser = function(req, res) {
    User.findById(req.params.id, function(err, user) {
        if(err) return res.status(500).send("No existe este usuario.");

        User.find({email: req.body.email}, function(err, users) {
            if(err) return res.send(500, err.message);
            if(users.length > 1) return res.status(500).send("Este email ya existe.");
            if(users.length === 1 && users[0].id !== req.params.id) return res.status(500).send("Este email ya existe.");
            
            if(user.password !== req.body.password){
                bcrypt.hash(req.body.password, config.salt)
                .then(function(password) {
                    user.nickname = req.body.nickname;
                    user.password = password;
                    user.role = req.body.role;
                    user.email = req.body.email;
                    
                    user.save(function(err, user) {
                        if(err) return res.status(500).send(err.message);
                        res.status(200).jsonp(user);
                    });
                });
            } else {
                user.nickname = req.body.nickname;
                user.role = req.body.role;
                user.email = req.body.email;
                
                user.save(function(err, user) {
                    if(err) return res.status(500).send(err.message);
                    res.status(200).jsonp(user);
                });
            }
        });
    });
};

//DELETE - Delete a user with specified ID
exports.deleteUser = function(req, res) {
    User.findById(req.params.id, function(err, user) {
        if(err) return res.status(500).send(err.message);
        if(!user) return res.status(500).send("No existe este usuario.");

        user.remove(function(err) {
            if(err) return res.status(500).send(err.message);
            Device.find({ user: user._id }, function (err, devices) {
                if (err) return res.status(500).send(err.message);
                devices.forEach(device => device.remove(function (err) {
                    if (err) return res.status(500).send(err.message);
                    Rule.find({ device: device._id }, function (err, rules) {
                        if (err) return res.status(500).send(err.message);
                        rules.forEach(rule => rule.remove(function (err) {
                            if (err) return res.status(500).send(err.message);
                        }))
                    });
                }))
            });
            res.status(200).jsonp(user);
        })
    });
};