/*
Copyright (C) 2020  Sergio Medeiros García

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*/
"use strict";

var bcrypt = require('bcrypt');
var jwt = require("jsonwebtoken");
var config = require("../config/config");
var User  = require("../models/user");

//POST - Login
exports.login = function(req, res) {
    User.find({email: req.body.email}, function(err, user) {
        if(err) return res.status(500).send(err.message);

        if(!user[0]){
            return res.status(500).send("Usuario no registrado.");
        } else {
            var user = user[0];
            bcrypt.compare(req.body.password, user.password)
            .then(function(comparation){
                if(comparation){
                    user = user.toJSON();
                    user["id"] = user._id;
                    delete user["_id"];
                    delete user["password"];
                    var token = jwt.sign(user, config.key, {expiresIn: 36000});
                    return res.status(200).send({user: user, token: token});
                } else {
                    return res.status(403).send("Contraseña incorrecta.");
                }
            });
        }
    });
}

//POST - Register
exports.register = function(req, res) {
    User.find({email: req.body.email}, function(err, user) {
        if(err) return res.status(500).send(err.message);

        if(!user[0]){ // If user does not exist
            bcrypt.hash(req.body.password, config.salt)
            .then(function(password) {
                var user = new User({
                    nickname: req.body.nickname,
                    email: req.body.email,
                    password: password,
                    role: req.body.role
                });
                
                user.save(function(err, user) {
                    if(err) return res.status(500).send(err.message);

                    user = user.toJSON();
                    user["id"] = user._id;
                    delete user["_id"];
                    delete user["password"];
                    var token = jwt.sign(user, config.key, {expiresIn: 36000});
                    return res.status(201).send({user: user, token: token});
                });
            });
        }else{ // User already exists
            res.status(500).send("Este usuario ya existe.");
        }
    });
}

// function to check if an user is authorized
exports.auth = function(req, res, next) {
    const token = req.headers['authorization'];

    if (token) {
        jwt.verify(token, config.key, function(err, decoded){      
          if (err) {
            return res.status(401).send(err.message);
          } else {
            req.decoded = decoded;    
            next();
          }
        });
    } else {
        return res.status(401).send("Token needed.");
    }
}