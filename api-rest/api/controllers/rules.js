/*
Copyright (C) 2020  Sergio Medeiros García

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*/
"use strict";

var Device = require("../models/device");
var User = require("../models/user");
var Rule = require("../models/rule");
var Group = require("../models/group");

//GET - Return all rules in the DB
exports.findAllRules = function (req, res) {
    Rule.find(function (err, rules) {
        if (err) return res.status(500).send(err.message);
        Device.populate(rules, { path: "device" }, function (err, rules) {
            if (err) return res.status(500).send(err.message);
            User.populate(rules, { path: "user" }, function (err, rules) {
                if (err) return res.status(500).send(err.message);
                Group.populate(rules, { path: "group" }, function (err, rules) {
                    if (err) return res.status(500).send(err.message);
                    res.status(200).jsonp(rules);
                });
            });
        });
    });
}

//GET - Return all rules to turn them on
exports.findRulesToTurnOn = function (req, res) {
    var today = new Date();
    today.setHours(0);
    today.setMinutes(0);
    today.setSeconds(0);
    today.setMilliseconds(0);
    var tomorrow = new Date(today.getFullYear() + "-" + (today.getMonth() + 1) + "-" + (today.getDate() + 1));
    var filter = {
        $and: [
            { active: true },
            { type: 'on' },
            {
                $or: [
                    {
                        executeAt: { $lt: tomorrow }
                    },
                    {
                        $and: [
                            { initDate: { $lt: tomorrow } },
                            { endDate: { $gte: today } }
                        ]
                    }
                ]
            }
        ]
    }
    Rule.find(filter, function (err, rules) {
        if (err) return res.status(500).send(err.message);
        Group.populate(rules, { path: "group" }, function (err, rules) {
            if (err) return res.status(500).send(err.message);
            Device.populate(rules, { path: "device group.devices" }, function (err, rules) {
                if (err) return res.status(500).send(err.message);
                res.status(200).jsonp(rules);
            });
        });
    });
}

//GET - Return all rules to turn them off
exports.findRulesToTurnOff = function (req, res) {
    var today = new Date();
    today.setHours(0);
    today.setMinutes(0);
    today.setSeconds(0);
    today.setMilliseconds(0);
    var tomorrow = new Date(today.getFullYear() + "-" + (today.getMonth() + 1) + "-" + (today.getDate() + 1));
    var filter = {
        $and: [
            { active: true },
            { type: 'off' },
            {
                $or: [
                    {
                        executeAt: { $lt: tomorrow }
                    },
                    {
                        $and: [
                            { initDate: { $lt: tomorrow } },
                            { endDate: { $gte: today } }
                        ]
                    }
                ]
            }
        ]
    }
    Rule.find(filter, function (err, rules) {
        if (err) return res.status(500).send(err.message);
        Group.populate(rules, { path: "group" }, function (err, rules) {
            if (err) return res.status(500).send(err.message);
            Device.populate(rules, { path: "device group.devices" }, function (err, rules) {
                if (err) return res.status(500).send(err.message);
                res.status(200).jsonp(rules);
            });
        });
    });
}

//GET - Return all non-periodic rules in the DB
exports.findPunctualRules = function (req, res) {
    Rule.find({ periodic: false }, function (err, rules) {
        if (err) return res.status(500).send(err.message);
        Device.populate(rules, { path: "device" }, function (err, rules) {
            if (err) return res.status(500).send(err.message);
            User.populate(rules, { path: "user" }, function (err, rules) {
                if (err) return res.status(500).send(err.message);
                Group.populate(rules, { path: "group" }, function (err, rules) {
                    if (err) return res.status(500).send(err.message);
                    res.status(200).jsonp(rules);
                });
            });
        });
    });
}

//GET - Return all periodic rules in the DB
exports.findPeriodicRules = function (req, res) {
    Rule.find({ periodic: true }, function (err, rules) {
        if (err) return res.status(500).send(err.message);
        Device.populate(rules, { path: "device" }, function (err, rules) {
            if (err) return res.status(500).send(err.message);
            User.populate(rules, { path: "user" }, function (err, rules) {
                if (err) return res.status(500).send(err.message);
                Group.populate(rules, { path: "group" }, function (err, rules) {
                    if (err) return res.status(500).send(err.message);
                    res.status(200).jsonp(rules);
                });
            });
        });
    });
}

//GET - Return a rule with specified ID
exports.findRuleByID = function (req, res) {
    Rule.findById(req.params.id, function (err, rule) {
        if (err) return res.status(500).send(err.message);
        Device.populate(rule, { path: "device" }, function (err, rule) {
            if (err) return res.status(500).send(err.message);
            User.populate(rule, { path: "user" }, function (err, rule) {
                if (err) return res.status(500).send(err.message);
                Group.populate(rule, { path: "group" }, function (err, rule) {
                    if (err) return res.status(500).send(err.message);
                    res.status(200).jsonp(rule);
                });
            });
        });
    });
};

//GET - Return user's rules
exports.findUserRules = function (req, res) {
    Rule.find({ user: req.params.id }, function (err, rule) {
        if (err) return res.status(500).send(err.message);
        Device.populate(rule, { path: "device" }, function (err, rule) {
            if (err) return res.status(500).send(err.message);
            User.populate(rule, { path: "user" }, function (err, rule) {
                if (err) return res.status(500).send(err.message);
                Group.populate(rule, { path: "group" }, function (err, rule) {
                    if (err) return res.status(500).send(err.message);
                    res.status(200).jsonp(rule);
                });
            });
        });
    });
};

//GET - Return non-periodic user's rules
exports.findPunctualUserRules = function (req, res) {
    Rule.find({ periodic: false, user: req.params.id }, function (err, rule) {
        if (err) return res.status(500).send(err.message);
        Device.populate(rule, { path: "device" }, function (err, rule) {
            if (err) return res.status(500).send(err.message);
            User.populate(rule, { path: "user" }, function (err, rule) {
                if (err) return res.status(500).send(err.message);
                Group.populate(rule, { path: "group" }, function (err, rule) {
                    if (err) return res.status(500).send(err.message);
                    res.status(200).jsonp(rule);
                });
            });
        });
    });
};

//GET - Return periodic user's rules
exports.findPeriodicUserRules = function (req, res) {
    Rule.find({ periodic: true, user: req.params.id }, function (err, rule) {
        if (err) return res.status(500).send(err.message);
        Device.populate(rule, { path: "device" }, function (err, rule) {
            if (err) return res.status(500).send(err.message);
            User.populate(rule, { path: "user" }, function (err, rule) {
                if (err) return res.status(500).send(err.message);
                Group.populate(rule, { path: "group" }, function (err, rule) {
                    if (err) return res.status(500).send(err.message);
                    res.status(200).jsonp(rule);
                });
            });
        });
    });
};

//GET - Return device's rules
exports.findDeviceRules = function (req, res) {
    Rule.find({ device: req.params.id }, function (err, rule) {
        if (err) return res.status(500).send(err.message);
        Device.populate(rule, { path: "device" }, function (err, rule) {
            if (err) return res.status(500).send(err.message);
            User.populate(rule, { path: "user" }, function (err, rule) {
                if (err) return res.status(500).send(err.message);
                Group.populate(rule, { path: "group" }, function (err, rule) {
                    if (err) return res.status(500).send(err.message);
                    res.status(200).jsonp(rule);
                });
            });
        });
    });
};


//GET - Return non-periodic device's rules
exports.findPunctualDeviceRules = function (req, res) {
    Rule.find({ periodic: false, device: req.params.id }, function (err, rule) {
        if (err) return res.status(500).send(err.message);
        Device.populate(rule, { path: "device" }, function (err, rule) {
            if (err) return res.status(500).send(err.message);
            User.populate(rule, { path: "user" }, function (err, rule) {
                if (err) return res.status(500).send(err.message);
                Group.populate(rule, { path: "group" }, function (err, rule) {
                    if (err) return res.status(500).send(err.message);
                    res.status(200).jsonp(rule);
                });
            });
        });
    });
};

//GET - Return periodic device's rules
exports.findPeriodicDeviceRules = function (req, res) {
    Rule.find({ periodic: true, device: req.params.id }, function (err, rule) {
        if (err) return res.status(500).send(err.message);
        Device.populate(rule, { path: "device" }, function (err, rule) {
            if (err) return res.status(500).send(err.message);
            User.populate(rule, { path: "user" }, function (err, rule) {
                if (err) return res.status(500).send(err.message);
                Group.populate(rule, { path: "group" }, function (err, rule) {
                    if (err) return res.status(500).send(err.message);
                    res.status(200).jsonp(rule);
                });
            });
        });
    });
};

//POST - Insert a new rule in the DB
exports.createRule = function (req, res) {

    var rule = new Rule({
        type: req.body.type,
        device: req.body.device,
        group: req.body.group,
        user: req.body.user,
        executeAt: req.body.executeAt,
        time: req.body.time,
        active: req.body.active,
        periodic: req.body.periodic,
        period: req.body.period,
        initDate: req.body.initDate,
        endDate: req.body.endDate,
        command: req.body.command
    });

    rule.save(function (err, rule) {
        if (err) return res.status(500).send(err.message);
        res.status(201).jsonp(rule);
    });
};

//PUT - Update a rule that already exists
exports.updateRule = function (req, res) {
    Rule.findById(req.params.id, function (err, rule) {
        if (err) return res.status(500).send(err.message);

        rule.type = req.body.type;
        rule.device = req.body.device;
        rule.group = req.body.group;
        rule.user = req.body.user;
        rule.executeAt = req.body.executeAt;
        rule.active = req.body.active;
        rule.time = req.body.time;
        rule.periodic = req.body.periodic;
        rule.period = req.body.period;
        rule.initDate = req.body.initDate;
        rule.endDate = req.body.endDate;
        rule.command = req.body.command;

        rule.save(function (err) {
            if (err) return res.status(500).send(err.message);
            res.status(200).jsonp(rule);
        });
    });
};

//PUT - Enable an existing rule
exports.enableRule = function (req, res) {
    Rule.findById(req.params.id, function (err, rule) {
        if (err) return res.status(500).send(err.message);

        rule.active = true;

        rule.save(function (err) {
            if (err) return res.status(500).send(err.message);
            res.status(200).jsonp(rule);
        });
    });
};

//PUT - Disable an existing rule
exports.disableRule = function (req, res) {
    Rule.findById(req.params.id, function (err, rule) {
        if (err) return res.status(500).send(err.message);

        rule.active = false;

        rule.save(function (err) {
            if (err) return res.status(500).send(err.message);
            res.status(200).jsonp(rule);
        });
    });
};

//DELETE - Delete a group with specified ID
exports.deleteRule = function (req, res) {
    Rule.findById(req.params.id, function (err, rule) {
        if (err) return res.status(500).send(err.message);
        if (!rule) return res.status(500).send("Regla no encontrada.");

        rule.remove(function (err) {
            if (err) return res.status(500).send(err.message);
            res.status(200).jsonp(rule);
        })
    });
};
