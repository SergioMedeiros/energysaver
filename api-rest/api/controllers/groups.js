/*
Copyright (C) 2020  Sergio Medeiros García

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*/
"use strict";

var Device  = require("../models/device");
var User  = require("../models/user");
var Group  = require("../models/group");

//GET - Return all groups in the DB
exports.findAllGroups = function(req, res) {
    Group.find(function(err, groups) {
        if(err) return res.status(500).send(err.message);
        Device.populate(groups, {path: "devices"}, function(err, groups) {
            if(err) return res.status(500).send(err.message);
            User.populate(groups, {path: "user"}, function(err, groups) {
                if(err) return res.status(500).send(err.message);
                res.status(200).jsonp(groups);
            });
        });
    });
}

//GET - Return a group with specified ID
exports.findGroupByID = function(req, res) {
    Group.findById(req.params.id, function(err, group) {
        if(err) return res.status(500).send(err.message);
        Device.populate(group, {path: "devices"}, function(err, group) {
            if(err) return res.status(500).send(err.message);
            User.populate(group, {path: "user"}, function(err, group) {
                if(err) return res.status(500).send(err.message);
                res.status(200).jsonp(group);
            });
        });
    });
};

//GET - Return all groups of a specific User
exports.findUserGroups = function(req, res) {
    Group.find({user: req.params.id}, function(err, groups) {
        if(err) return res.status(500).send(err.message);
        Device.populate(groups, {path: "devices"}, function(err, groups) {
            if(err) return res.status(500).send(err.message);
            User.populate(groups, {path: "user"}, function(err, groups) {
                if(err) return res.status(500).send(err.message);
                res.status(200).jsonp(groups);
            });
        });
    });
};

//POST - Insert a new group in the DB
exports.createGroup = function(req, res) {
    Group.find({user: req.body.user, name: req.body.name}, function(err, group) {
        if(err) return res.status(500).send(err.message);
        
        if(!group[0]){
            var group = new Group({
                name:         req.body.name,
                description:  req.body.description,
                devices:      req.body.devices,
                user:         req.body.user
            });
        
            group.save(function(err, group) {
                if(err) return res.status(500).send(err.message);
                res.status(201).jsonp(group);
            });
        }else{
            res.status(200).send("Este grupo ya existe.");
        }
    });
};

//PUT - Update a group that already exists
exports.updateGroup = function(req, res) {
    Group.findById(req.params.id, function(err, group) {
        if(err) return res.status(500).send("Grupo no encontrado.");

        if(group.name === req.body.name && group.user.toString() === req.body.user){
            group.description = req.body.description;
            group.devices = req.body.devices;

            group.save(function(err) {
                if(err) return res.status(500).send(err.message);
                res.status(200).jsonp(group);
            });
        } else {
            Group.find({user: req.body.user, name: req.body.name}, function(err, another_group) {
                if(err) return res.send(500, err.message);
                
                if(another_group[0]){
                    res.status(200).send("Este grupo ya existe.");
                }else{
                    group.name =  req.body.name;
                    group.description = req.body.description;
                    group.devices = req.body.devices;
                    group.user = req.body.user;

                    group.save(function(err) {
                        if(err) return res.status(500).send(err.message);
                        res.status(200).jsonp(group);
                    });
                }
            });
        }
    });
};

//PUT - Exclude a device of a group
exports.freeGroupDevice = function (req, res) {
    Group.findById(req.params.id, function (err, group) {
        if (err) return res.status(500).send("Grupo no encontrado.");

        var index = group.devices.indexOf(req.body.device);
        if (index > -1) {
            group.devices.splice(index, 1);
        }

        group.save(function (err) {
            if (err) res.status(500).send(err.message);
            res.status(200).jsonp(group);
        });
    });
};

//DELETE - Delete a group with specified ID
exports.deleteGroup = function(req, res) {
    Group.findById(req.params.id, function(err, group) {
        if(err) return res.send(500, err.message);
        if(!group) return res.status(500).send("Grupo no encontrado.");

        group.remove(function(err) {
            if(err) return res.status(500).send(err.message);
            res.status(200).jsonp(group);
        })
    });
};