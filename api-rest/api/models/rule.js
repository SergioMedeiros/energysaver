/*
Copyright (C) 2020  Sergio Medeiros García

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*/
"use strict";

var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var ruleShcema = new Schema({
  type:         { type: String, required: true }, // on/off
  device:       { type: Schema.ObjectId, ref: "device" },
  group:        { type: Schema.ObjectId, ref: "group" },
  user:         { type: Schema.ObjectId, ref: "user", required: true },
  time:         { type: Date },
  executeAt:    { type: Date },
  active:       { type: Boolean, default: true }, // boolean to know if still active or it is done
  periodic:     { type: Boolean, default: false },// boolean to know if it is periodic
  period:       [{ type: Boolean }],              // period --> all mondays / once per month / each hour
  initDate:     { type: Date },
  endDate:      { type: Date },
  command:      { type: String }
}, {
  timestamps: true
});

module.exports = mongoose.model("rule", ruleShcema);