/*
Copyright (C) 2020  Sergio Medeiros García

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*/
"use strict";

var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var groupShcema = new Schema({
  name:         { type: String, required: true },
  description:  { type: String },
  devices:      [{ type: Schema.ObjectId, ref: "device" }],
  user:         { type: Schema.ObjectId, ref: "user", required: true}
}, {
    timestamps: true
});

module.exports = mongoose.model("group", groupShcema);