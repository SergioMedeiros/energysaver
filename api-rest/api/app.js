/*
Copyright (C) 2020  Sergio Medeiros García

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*/
"use strict";

// Loading modules
var express = require("express");
var cors = require("cors");
var bodyParser = require("body-parser");
var methodOverride = require("method-override");

// Initializing express
var app = express();

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(methodOverride());

// Controllers
var users_controller = require('./controllers/users');
var devices_controller = require('./controllers/devices');
var groups_controller = require('./controllers/groups');
var rules_controller = require('./controllers/rules');
var access_controller = require('./controllers/access');

// Dfine new router
var router = express.Router();

// Setting our routes
// Access routes 
router.route('/login')
  .post(access_controller.login);

router.route('/register')
  .post(access_controller.register);

// User routes 
router.route('/users')
  .get(access_controller.auth, users_controller.findAllUsers)
  .post(access_controller.auth, users_controller.createUser);

router.route('/users/:id')
  .get(access_controller.auth, users_controller.findUserByID)
  .put(access_controller.auth, users_controller.updateUser)
  .delete(access_controller.auth, users_controller.deleteUser);

router.route('/users/:id/devices')
  .get(access_controller.auth, devices_controller.findUserDevices);

router.route('/users/:id/groups')
  .get(access_controller.auth, groups_controller.findUserGroups);

router.route('/users/:id/rules')
  .get(access_controller.auth, rules_controller.findUserRules);

router.route('/users/:id/rules/punctual')
  .get(access_controller.auth, rules_controller.findPunctualUserRules);

router.route('/users/:id/rules/periodic')
  .get(access_controller.auth, rules_controller.findPeriodicUserRules);

// Device routes
router.route('/devices')
  .get(access_controller.auth, devices_controller.findAllDevices)
  .post(access_controller.auth, devices_controller.createDevice);

router.route('/devices/:id')
  .get(access_controller.auth, devices_controller.findDeviceByID)
  .put(access_controller.auth, devices_controller.updateDevice)
  .delete(access_controller.auth, devices_controller.deleteDevice);

router.route('/devices/:id/group')
  .put(access_controller.auth, devices_controller.addDeviceGroup);

router.route('/devices/:id/free')
  .put(access_controller.auth, devices_controller.freeDeviceGroup);

router.route('/devices/:id/rules')
  .get(rules_controller.findDeviceRules);

router.route('/devices/:id/rules/punctual')
  .get(rules_controller.findPunctualDeviceRules);

router.route('/devices/:id/rules/periodic')
  .get(rules_controller.findPeriodicDeviceRules);

// Group routes
router.route('/groups')
  .get(access_controller.auth, groups_controller.findAllGroups)
  .post(access_controller.auth, groups_controller.createGroup);

router.route('/groups/:id')
  .get(access_controller.auth, groups_controller.findGroupByID)
  .put(access_controller.auth, groups_controller.updateGroup)
  .delete(access_controller.auth, groups_controller.deleteGroup);

router.route('/groups/:id/devices')
  .get(access_controller.auth, devices_controller.findFreeDevices)

router.route('/groups/:id/free')
  .put(access_controller.auth, groups_controller.freeGroupDevice);

router.route('/groups/:id/devices/:user')
  .get(access_controller.auth, devices_controller.findFreeDevicesFor)

// Rules routes
router.route('/rules')
  .get(access_controller.auth, rules_controller.findAllRules)
  .post(access_controller.auth, rules_controller.createRule);

router.route('/rules/to-turn-on')
  .get(rules_controller.findRulesToTurnOn);

router.route('/rules/to-turn-off')
  .get(rules_controller.findRulesToTurnOff);

router.route('/rules/punctual')
  .get(access_controller.auth, rules_controller.findPunctualRules);

router.route('/rules/periodic')
  .get(access_controller.auth, rules_controller.findPeriodicRules);

router.route('/rules/:id')
  .get(access_controller.auth, rules_controller.findRuleByID)
  .put(access_controller.auth, rules_controller.updateRule)
  .delete(access_controller.auth, rules_controller.deleteRule);

router.route('/rules/:id/enable')
  .put(access_controller.auth, rules_controller.enableRule);

router.route('/rules/:id/disable')
  .put(access_controller.auth, rules_controller.disableRule);

router.route('/rules/:id/finish')
  .put(rules_controller.disableRule);

// Base path
app.use('/api', router);

// export this module
module.exports = app;
