# EnergySaver

Proyecto para crear una infraestructura que permita la programación del encendido y apagado de equipos remotos.

## Resumen

El proyecto se divide en:

- Api-rest ->		    API desarrollada con NodeJS y Expressjs para conectarse con base de datos MongoDB.
- Client-config ->	    Ficheros de configuración de cliente.
- Server-config ->	    Ficheros de configuración del servidor.
- Web-application -> 	Aplicación web desarrollada en Angular..

### Prerequisitos

He usado un sistema operativo CentOS 8 como servidor energysaver, se puede descargar su ISO desde [aquí](https://www.centos.org/download/).

### Instalación del cliente

Cualquier dispositivo conectado a la misma red local del servidor puede ser un cliente energysaver, en este caso he desarrollado un instalador para un sistema operativo Ubuntu 18.04, para instalar el servidor basta con clonar el repositorio y ejecutar; `make build-client`.

En caso de que el dispositivo disponga de un sistema operativo diferente, habrá de adaptar los scripts a su sistema operativo.

### Instalación del servidor

**Hay que tener en cuenta de que se ha usado [CentOS 8](https://www.centos.org/download/) como sistema operativo base para el servidor**, se recomienda encarecidamente que se use el mismo sistema operativo, ya que de otra forma, los scripts podrían no funcionar.

La aplicación web se despliega sobre apache y se conecta con la base de datos MongoDB a través la API RESTful, para instalar el servidor basta con clonar el repositorio en tu CentOS 8 y ejecutar; `make build-server`.

## Tecnología

* [Angular](https://angular.io/docs) - Aplicación Web
* [NodeJS](https://nodejs.org/es/docs/) - API RESTful
* [MongoDB](https://docs.mongodb.com/) - Base de datos 
* [Apache](https://httpd.apache.org/) - Servidor web

## Authors

* **Sergio Medeiros**  - [GitLab](https://gitlab.com/SergioMedeiros)

## Acknowledgments

* [UBU](https://www.ubu.es/)
